package com.dunkan.restaurant.controller;

import java.sql.SQLException;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.dunkan.restaurant.connection.MySQL;
import com.dunkan.restaurant.connection.MySQLHandler;
import com.dunkan.restaurant.producto.Product;

/**
 * Listado de productos
 * @author dunkansdk
 *
 */
public class ProductController {
	
	private static HashMap<Integer, Product> productos;
	private MySQLHandler handler;
	
	//
	// TODO: ESTO SE TIENE QUE CARGAR DESDE LA DB!
	//
	public ProductController() {
		handler = new MySQLHandler(new MySQL());
		productos = new HashMap<Integer, Product>();
	}
	
	/**
	 * <p>Agrega un producto al mapa con un codigo determinado</p>
	 * @param codigo
	 * @param producto
	 */
	public void add(Integer codigo, Product producto) {
		productos.put(codigo,  producto);
		
		try {
			handler.getMySQL().updateSQL("INSERT INTO product " + "VALUES (" + codigo + ",'" + producto.getDescripcion() + 
														"'," + producto.getPrecio() + "," + producto.getFecha() +
														"," + producto.getStock() + "," + producto.getStock() + ")");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * <p>Agrega un producto al mapa</p>
	 * @param producto
	 */
	public void add(Product producto) {
		
		// TODO: Despues de estudiar para empresarial!!!!!!!
		try {
			handler.getMySQL().updateSQL("INSERT INTO product " + "VALUES (" + getCode() + ",'" + producto.getDescripcion() + 
														"'," + Date.from(producto.getFecha().atStartOfDay(ZoneId.systemDefault()).toInstant()) + 
														"," + producto.getPrecio() + "," + producto.getStock() + "," + producto.getStock() + ")");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		productos.put(getCode(),  producto);
	}
	
	/**
	 * <p>Remueve un codigo del mapa</p>
	 * @param codigo
	 */
	public void delete(Integer codigo) {
		Iterator<Map.Entry<Integer, Product>> it = productos.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry<Integer, Product> pair = (Map.Entry<Integer, Product>) it.next();
			if(pair.getKey().equals(codigo)) it.remove();
		}
	}
	
	/**
	 * <p>Verifica si ya se ha asignado determinado codigo a otro producto</p>
	 * @param codigo
	 * @return boolean
	 */
	public boolean exist(Integer codigo) {
		for(Map.Entry<Integer, Product> entry : productos.entrySet()) {
			if(entry.getKey().equals(codigo)) return true;
		}
		return false;
	}
	
	/**
	 * <p>Modifica la descripcion del producto</p>
	 * @param codigo
	 * @param descripcion
	 */
	public void modify(Integer codigo, String descripcion) {
		productos.get(codigo).setDescripcion(descripcion);
	}

	/**
	 * <p>Modifica el precio del producto</p>
	 * @param codigo
	 * @param precio
	 */
	public void modifyPrice(Integer codigo, double precio) {
		productos.get(codigo).setPrecio(precio);
	}
	
	/**
	 * <p>Modifica el precio <b>[COSTO]</b> del producto</p>
	 * @param codigo
	 * @param precio
	 */
	public void modifyCost(Integer codigo, double costo) {
		productos.get(codigo).setCosto(costo);
	}
	
	/**
	 * <p>Modifica el stock actual del producto</p>
	 * @param codigo
	 * @param costo
	 */
	public void modifyStock(Integer codigo, int stock) {
		productos.get(codigo).setStock(stock);
	}
	
	/**
	 * <p>Obtiene el producto a partir del ID</p>
	 * @see {@link com.dunkan.restaurant.controller.ProductController#productos}
	 * @param id
	 * @return
	 */
	public Product getByID(Integer id) {
		return productos.get(id);		
	}
	
	/**
	 * <p>Obtiene el producto a partir de la descripcion del mismo</p>
	 * @see {@link com.dunkan.restaurant.producto.Product#getDescripcion()}
	 * @param description
	 * @return
	 */
	public Product getByDescription(String description) {
		for(Map.Entry<Integer, Product> entry : productos.entrySet()) {
			if(entry.getValue().getDescripcion().equals(description)) return entry.getValue();
		}
		return null;
	}
	
	/**
	 * <p>Obtiene el proximo codigo a asignar secuencialmente</p>
	 * @return
	 */
	private Integer getCode() {
		return productos.size() + 1;
	}

	public String debug() {
		return "ProductController" + productos;
	}
	
}
