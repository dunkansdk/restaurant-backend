package com.dunkan.restaurant.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import com.dunkan.restaurant.proveedor.Pago;
import com.dunkan.restaurant.proveedor.Proveedor;

public class PagoController {
	
	private HashMap<Proveedor, ArrayList<Pago>> pagos;
	
	//
	// TODO: ESTO SE TIENE QUE CARGAR DE LA DB
	//
	public PagoController() {
		pagos = new HashMap<Proveedor, ArrayList<Pago>>();
	}
	
	/**
	 * <p>Agrega un pago ya instanciado a un proveedor</p>
	 * @param pago
	 * @param proveedor
	 */
	public void add(Pago pago, Proveedor proveedor) {
		ArrayList<Pago> list = pagos.get(proveedor);
		
		// Si no existe el proveedor lo agrega
		if(list == null) {
			list = new ArrayList<Pago>();
			list.add(pago);
			pagos.put(proveedor, list);
		} else {
			list.add(pago);
		}
	}
	
	/**
	 * <p>Instancia un pago y lo asocia a un proveedor</p>
	 * @param fecha
	 * @param boleta
	 * @param proveedor
	 */
	public void add(LocalDate fecha, int boleta, Proveedor proveedor) {
		ArrayList<Pago> list = pagos.get(proveedor);
		
		// Si no existe el proveedor lo agrega
		if(list == null) {
			list = new ArrayList<Pago>();
			list.add(new Pago(fecha, boleta));
			pagos.put(proveedor, list);
		} else {
			list.add(new Pago(fecha, boleta));
		}		
	}
	
	/**
	 * <p>Obtiene el historial de pagos hechos a un proveedor</p>
	 * @param proveedor
	 * @return ArrayList<Pago> {@link com.dunkan.restaurant.proveedor.Pago}
	 */
	public ArrayList<Pago> obtain(Proveedor proveedor) {
		return pagos.get(proveedor);
	}

}
