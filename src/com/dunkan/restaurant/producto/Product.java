package com.dunkan.restaurant.producto;

import java.io.Serializable;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashMap;

import com.dunkan.restaurant.connection.MySQL;
import com.dunkan.restaurant.connection.MySQLHandler;

/**
 * Producto que se ofrece a la venta
 *
 */
@SuppressWarnings("serial")
public class Product implements Serializable {
		
	private String descripcion;
	private LocalDate fecha;
	private double precio;
	private double costo;
	private int stock;
	
	/**
	 * <p>Historial de precios / modificaciones de determinado producto</p>
	 * <p>(Fecha de la modificacion, precio)</p>
	 */
	private HashMap<LocalDate, Double> historial;
	
	/**
	 * <p>Crea un nuevo producto en el 'catalogo' (Sin un historial de modificacion de precios)</p>
	 * @param codigo
	 * @param descripcion
	 * @param fecha
	 * @param precio
	 */
	public Product(String descripcion, LocalDate fecha, double precio, double costo) {
		historial = new HashMap<LocalDate, Double>();
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.precio = precio;		
	}
	
	/**
	 * <p>Al cargar un producto que ya fue creado previamente, se le permite agregar el historial de precios guardado</p>
	 * @param codigo
	 * @param descripcion
	 * @param fecha
	 * @param precio
	 * @param historial
	 */
	public Product(int codigo, String descripcion, LocalDate fecha, double precio, HashMap<LocalDate, Double> historial) {
		this.historial = historial;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.precio = precio;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public double getPrecio() {
		return precio;
	}
	
	public HashMap<LocalDate, Double> getHistorial() {
		return historial;
	}
		
	public double getCosto() {
		return costo;
	}

	public void setCosto(double costo) {
		this.costo = costo;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public void setPrecio(double precio) {
		historial.put(LocalDate.now(), precio);
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "Producto [descripcion=" + descripcion + ", fecha=" + fecha + ", precio=" + precio + ", historial="
				+ historial + "]\n";
	}

}
