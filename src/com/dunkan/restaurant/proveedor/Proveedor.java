package com.dunkan.restaurant.proveedor;

/**
 * Proveedor
 */
public class Proveedor {
	
	private String nombre;
	private String mail;
	private String cuit;
	private int telefono;
	
	public Proveedor(String nombre, String mail, String cuit, int telefono) {
		this.nombre = nombre;
		this.mail = mail;
		this.cuit = cuit;
		this.telefono = telefono;
	}

	public String getNombre() {
		return nombre;
	}

	public String getMail() {
		return mail;
	}

	public String getCuit() {
		return cuit;
	}

	public int getTelefono() {
		return telefono;
	}

}
