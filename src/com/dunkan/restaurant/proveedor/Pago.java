package com.dunkan.restaurant.proveedor;

import java.time.LocalDate;

/**
 * Son los pagos que se emiten desde la sucursal (por lo general a proveedores)
 */
public class Pago {

	private LocalDate fecha;
	private int boleta;
	private double debitos;
	private double pagos;
	
	public Pago() {}
	
	public Pago(LocalDate fecha, int boleta) {
		this.boleta = boleta;
		this.fecha = fecha;
	}
	
	public Pago(LocalDate fecha, int boleta, double debitos, double pagos) {
		this.fecha = fecha;
		this.boleta = boleta;
		this.debitos = debitos;
		this.pagos = pagos;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public int getBoleta() {
		return boleta;
	}

	public void setBoleta(int boleta) {
		this.boleta = boleta;
	}

	public double getDebitos() {
		return debitos;
	}

	public void setDebitos(double debitos) {
		this.debitos = debitos;
	}

	public double getPagos() {
		return pagos;
	}

	public void setPagos(double pagos) {
		this.pagos = pagos;
	}
	
	/**
	 * <p>Compara el numero de boleta para saber si estamos tratando con el mismo pago</p>
	 */
	public boolean equals(Object object) {
		if(object instanceof Pago && object != null) {
			Pago pago = (Pago)object;
			if(this.boleta == pago.getBoleta()) {
				return true;
			}
		}
		return false;
	}

}
