package com.dunkan.restaurant.connection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class MySQLConnection {
	
	protected Connection connection;
	
	/**
	 * <p>Verifica si la conexion esta establecida con la base de datos</p>
	 *
	 * @return true si la conexion esta activa
	 * @throws SQLException if the connection cannot be checked
	 */
	public boolean checkConnection() throws SQLException {
		return this.connection != null && !this.connection.isClosed();
	}

	/**
	 * <p>Cierra la conexion con la base de datos</p>
	 *
	 * @return true si se cerro correctamente
	 * @throws SQLException en caso de que la conexion no este abierta
	 */
	public boolean closeConnection() throws SQLException {
		if (this.connection == null) return false;
		this.connection.close();
		return true;
	}

	/**
	 * <p>Obtiene la conexion activa con la base de datos</p>
	 *
	 * @return Conexion con la base de datos, null en caso de ser falsa
	 */
	public Connection getConnection() {
		return this.connection;
	}
	
	/**
	 * <p>Abre una conexion con una base de datos</p>
	 *
	 * @return Opened connection
	 * @throws SQLException if the connection can not be opened
	 * @throws ClassNotFoundException if the driver cannot be found
	 */
	public abstract Connection openConnection() throws SQLException, ClassNotFoundException;
	
	/**
	 * <p>Ejecuta un SQL Query</p>
	 * <p>See {@link java.sql.ResultSet)}</p>
	 * <p>Si la conexion esta cerrada, la intenta abrir</p>
	 *
	 * @param query Query que va a enviarse
	 * @return El resultado del query enviado
	 * @throws SQLException Si el query no fue ejecutado
	 * @throws ClassNotFoundException Si el driver no pudo encontrarse; see {@link #openConnection()}
	 */
	public ResultSet querySQL(String query) throws SQLException, ClassNotFoundException {
		if (!this.checkConnection()) this.openConnection();
		Statement statement = this.connection.createStatement();
		return statement.executeQuery(query);
	}

	/**
	 * <p>Ejecuta un SQL Query</p>
	 * <p>See {@link java.sql.Statement#executeUpdate(String)}</p>
	 * <p>Si la conexion esta cerrada, la intenta abrir</p>
	 *
	 * @param query Query que va a enviarse
	 * @return El resultado del query enviado, see {@link java.sql.Statement#executeUpdate(String)}
	 * @throws SQLException Si el query no fue ejecutado
	 * @throws ClassNotFoundException Si el driver no pudo encontrarse; see {@link #openConnection()}
	 */
	public int updateSQL(String query) throws SQLException, ClassNotFoundException {
		if (!this.checkConnection()) this.openConnection();
		Statement statement = this.connection.createStatement();
		return statement.executeUpdate(query);
	}

}
