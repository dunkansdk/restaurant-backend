package com.dunkan.restaurant.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQL extends MySQLConnection {
	
	private final String hostname;
	private final int port;
	private final String username;
	private final String password;
	private final String database;
	
	private static final String PRODUCT_TABLE = "CREATE TABLE product" + 
												"(id INTEGER," + 
												"description VARCHAR(50)," + 
												"fecha DATE," +
												"price DOUBLE," + 
												"cost DOUBLE," + 
												"stock INTEGER)";

	private boolean enabled = false;
	
	public MySQL() {
		this("localhost", 3306, "root", "", "restaurant");
	}

	public MySQL(String hostname, int port, String username, String password, String database) {
		this.hostname = hostname;
		this.port = port;
		this.username = username;
		this.password = password;
		this.database = database;
	}

	public String getDatabase() {
		return this.database;
	}

	public String getHostname() {
		return this.hostname;
	}

	public String getPassword() {
		return this.password;
	}

	public int getPort() {
		return this.port;
	}

	public String getUsername() {
		return this.username;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public MySQL setEnabled(boolean flag) {
		this.enabled = flag;
		return this;
	}
	
	/**
	 * 
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	@Override
	public Connection openConnection() throws SQLException, ClassNotFoundException {
		if (this.checkConnection()) return this.connection;
		this.connection = DriverManager.getConnection("jdbc:mysql://" + this.hostname + ":" + this.port + "/" + this.database + "?serverTimezone=GMT", this.username, this.password);		
		
		//
		// Verifica que la base de datos exista, en el caso de no ser asi, la crea.
		//
		try {			
			if(this.querySQL("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" + database + "'").next()) {
				System.out.println("La base de datos existe");
			} else {
				this.updateSQL("CREATE DATABASE " + database);
				System.err.println("La base de datos no existe, fue creada");
			}			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
					
		return this.connection;
	}
	
	/**
	 * 
	 */
	public void checkDatabase() {
		
		try {
			this.querySQL("SELECT * FROM product");
		} catch (ClassNotFoundException | SQLException e) {
			//e.printStackTrace();
			try {
				this.updateSQL(PRODUCT_TABLE);
			} catch (ClassNotFoundException | SQLException e1) {
				e1.printStackTrace();
			}
		}
		
	}

}
