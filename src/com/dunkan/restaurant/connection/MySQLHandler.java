package com.dunkan.restaurant.connection;

import java.sql.Connection;
import java.sql.SQLException;

public class MySQLHandler extends MySQLConnection {
	
	private MySQL mySQL;
	
	/**
	 * 
	 * @param data
	 */
	public MySQLHandler(MySQL data) {
		this.mySQL = data;
		this.connection = null;
		mySQL.checkDatabase();
	}
	
	/**
	 * <p>Verifica si la conexion esta establecida con la base de datos</p>
	 *
	 * @return true si la conexion esta activa
	 */
	public boolean checkConnection() {
		try {
			return this.mySQL.checkConnection();
		} catch (SQLException e) {
			//e.printStackTrace();
			System.err.println("[ERROR] checkConnection() on MySQLHandler class\n");
			return false;
		}
	}

	/**
	 * 
	 */
	public boolean closeConnection() {
		try {
			return this.mySQL.getConnection() != null && this.mySQL.closeConnection();
		} catch (SQLException e) {
			//e.printStackTrace();
			System.err.println("[ERROR] closeConnection() on MySQLHandler class\n");
			return false;
		}
	}
	
	/**
	 * 
	 */
	public Connection openConnection() {
		try {
			connection = this.mySQL.openConnection();
			System.out.println("Connected");
		} catch (ClassNotFoundException | SQLException e) {
			//e.printStackTrace();
			System.err.println("[ERROR] openConnection() on MySQLHandler class\n");
		}
		return connection;
	}
	
	public MySQL getMySQL() {
		return mySQL;
	}
}
