package com.dunkan.restaurant;

import java.time.LocalDate;

import com.dunkan.restaurant.controller.ProductController;
import com.dunkan.restaurant.producto.Product;

public class Main {
	
	private static ProductController pc = new ProductController();
	
	public static void main(String[] args) {
		pc.add(new Product("Cubierto", LocalDate.now(), 10.0, 0));
	}

}
